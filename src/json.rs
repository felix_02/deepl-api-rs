use crate::data::SourceLanguage;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct TranslationsResponse {
    translations: Vec<Translation>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct Translation {
    detected_source_language: SourceLanguage,
    text: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Usage {
    character_count: usize,
    character_limit: usize,
}

impl Usage {
    /// Get the usage's character count.
    #[must_use]
    pub fn character_count(&self) -> usize {
        self.character_count
    }

    /// Get the usage's character limit.
    #[must_use]
    pub fn character_limit(&self) -> usize {
        self.character_limit
    }
}

impl Translation {
    /// Get a reference to the translation's detected source language.
    #[must_use]
    pub fn detected_source_language(&self) -> &SourceLanguage {
        &self.detected_source_language
    }

    /// Get a reference to the translation's text.
    #[must_use]
    pub fn text(&self) -> &str {
        self.text.as_ref()
    }
}

impl TranslationsResponse {
    pub fn translations(&self) -> &Vec<Translation> {
        self.translations.as_ref()
    }

    pub fn first(&self) -> Option<&Translation> {
        if self.translations.len() == 0 {
            return None;
        }

        return Some(&self.translations[0]);
    }
}
