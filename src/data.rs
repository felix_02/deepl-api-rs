use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy)]
pub enum TargetLanguage {
    Bulgarian,
    Czech,
    Danish,
    German,
    Greek,
    EnglishBritish,
    EnglishAmerican,
    Spanish,
    Estonian,
    Finnish,
    French,
    Hungarian,
    Italian,
    Japanese,
    Lithuanian,
    Latvian,
    Dutch,
    Polish,
    Portuguese,
    Romanian,
    Russian,
    Slovak,
    Slovenian,
    Swedish,
    Chinese,
}

#[derive(Debug, PartialEq, Eq, Deserialize, Serialize, Clone)]
pub enum SourceLanguage {
    #[serde(rename = "BG")]
    Bulgarian,
    #[serde(rename = "CS")]
    Czech,
    #[serde(rename = "DA")]
    Danish,
    #[serde(rename = "DE")]
    German,
    #[serde(rename = "EL")]
    Greek,
    #[serde(rename = "EN")]
    English,
    #[serde(rename = "ES")]
    Spanish,
    #[serde(rename = "ET")]
    Estonian,
    #[serde(rename = "FI")]
    Finnish,
    #[serde(rename = "FR")]
    French,
    #[serde(rename = "HU")]
    Hungarian,
    #[serde(rename = "IT")]
    Italian,
    #[serde(rename = "JA")]
    Japanese,
    #[serde(rename = "LT")]
    Lithuanian,
    #[serde(rename = "LV")]
    Latvian,
    #[serde(rename = "NL")]
    Dutch,
    #[serde(rename = "PL")]
    Polish,
    #[serde(rename = "PT")]
    Portuguese,
    #[serde(rename = "RO")]
    Romanian,
    #[serde(rename = "RU")]
    Russian,
    #[serde(rename = "SK")]
    Slovak,
    #[serde(rename = "SL")]
    Slovenian,
    #[serde(rename = "SV")]
    Swedish,
    #[serde(rename = "ZH")]
    Chinese,
}

pub(crate) trait PrepareRequest {
    fn prepare_request(&self, req: ureq::Request) -> ureq::Request;
}

#[derive(Debug)]
pub struct TranslationRequest {
    texts: Vec<String>,
    source_lang: Option<SourceLanguage>,
    target_lang: TargetLanguage,
}

#[derive(Debug)]
pub struct EmptyRequest;

impl TranslationRequest {
    pub fn new<T: ToString>(text: T, target_lang: TargetLanguage) -> Self {
        Self {
            texts: vec![text.to_string()],
            source_lang: None,
            target_lang,
        }
    }

    pub fn add_text<T: ToString>(mut self, text: T) -> Self {
        self.texts.push(text.to_string());
        self
    }

    pub fn set_source_lang(mut self, lang: SourceLanguage) -> Self {
        self.source_lang = Some(lang);
        self
    }

    pub fn set_target_lang(mut self, lang: TargetLanguage) -> Self {
        self.target_lang = lang;
        self
    }
}

impl PrepareRequest for TranslationRequest {
    fn prepare_request(&self, mut req: ureq::Request) -> ureq::Request {
        req = req.query("target_lang", self.target_lang.as_str());

        if let Some(src_lang) = &self.source_lang {
            req = req.query("source_lang", src_lang.as_str());
        }

        for text in &self.texts {
            req = req.query("text", text);
        }

        req
    }
}

impl PrepareRequest for EmptyRequest {
    fn prepare_request(&self, req: ureq::Request) -> ureq::Request {
        req
    }
}

impl TargetLanguage {
    pub fn as_str(&self) -> &'static str {
        match &self {
            Self::Bulgarian => "BG",
            Self::Czech => "CS",
            Self::Danish => "DA",
            Self::German => "DE",
            Self::Greek => "EL",
            Self::EnglishBritish => "EN-GB",
            Self::EnglishAmerican => "EN-US",
            Self::Spanish => "ES",
            Self::Estonian => "ET",
            Self::Finnish => "FI",
            Self::French => "FR",
            Self::Hungarian => "HU",
            Self::Italian => "IT",
            Self::Japanese => "JA",
            Self::Lithuanian => "LT",
            Self::Latvian => "LV",
            Self::Dutch => "NL",
            Self::Polish => "PL",
            Self::Portuguese => "PT",
            Self::Romanian => "RO",
            Self::Russian => "RU",
            Self::Slovak => "SK",
            Self::Slovenian => "SL",
            Self::Swedish => "SV",
            Self::Chinese => "ZH",
        }
    }
}

impl SourceLanguage {
    pub fn as_str(&self) -> &'static str {
        match &self {
            Self::Bulgarian => "BG",
            Self::Czech => "CS",
            Self::Danish => "DA",
            Self::German => "DE",
            Self::Greek => "EL",
            Self::English => "EN",
            Self::Spanish => "ES",
            Self::Estonian => "ET",
            Self::Finnish => "FI",
            Self::French => "FR",
            Self::Hungarian => "HU",
            Self::Italian => "IT",
            Self::Japanese => "JA",
            Self::Lithuanian => "LT",
            Self::Latvian => "LV",
            Self::Dutch => "NL",
            Self::Polish => "PL",
            Self::Portuguese => "PT",
            Self::Romanian => "RO",
            Self::Russian => "RU",
            Self::Slovak => "SK",
            Self::Slovenian => "SL",
            Self::Swedish => "SV",
            Self::Chinese => "ZH",
        }
    }
}
