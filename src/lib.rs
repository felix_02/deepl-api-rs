//! # DeepL
//! This crate provides a connection to the DeepL API for Rust.
//! An API token is required for use, and an account at DeepL is needed for this.
//! This can be created [here](https://www.deepl.com/pro-api?cta=header-pro-api). Currently it is only tested with the free version,
//! but the paid version should also work.
//! 
//! ## Examle
//! ```
//! use deepl;
//! 
//! let deepl_client = deepl::DeepL::new("<API-Token>");
//! 
//! let translation = match deepl_client.translate_text(
//!     "Hello world", deepl::TargetLanguage::German
//! ) {
//!     Ok(res) => res,
//!     Err(err) => panic!("{:?}", err),
//! };
//! 
//! if let Some(translation) = translation {
//!     assert_eq!(translation.text(), "Hallo Welt");
//!     assert_eq!(translation.detected_source_language(), &deepl::SourceLanguage::English);
//! } else {
//!     assert!(false, "Should not occur");
//! }
//! ```
extern crate ureq;

mod data;
pub mod error;
mod json;

use data::{EmptyRequest, PrepareRequest};
pub use data::{SourceLanguage, TargetLanguage, TranslationRequest};
use error::{Error, Result};
pub use json::{Translation, TranslationsResponse, Usage};

const URL_API_FREE: &'static str = "https://api-free.deepl.com/v2";
const URL_API: &'static str = "https://api.deepl.com/v2";

/// The DeepL Client
#[derive(Debug, PartialEq, Eq)]
pub struct DeepL {
    auth_key: String,
    req_url: &'static str,
}

impl DeepL {
    /// Constructs a new Instance
    /// 
    /// ## Args
    /// - api_key: Your API-token
    /// 
    /// ## Example
    /// ```
    /// use deepl::DeepL;
    /// 
    /// let deepl_client = DeepL::new("<API-Token>");
    /// ```
    pub fn new<T: ToString>(api_key: T) -> Self {
        let auth_key = api_key.to_string();

        Self {
            req_url: match &auth_key.ends_with("fx") {
                true => URL_API_FREE,
                false => URL_API,
            },
            auth_key,
        }
    }

    /// Translates a text to the target language
    /// 
    /// ## Args
    /// - text: The text you want to translate
    /// - target_lang: The target language
    /// 
    pub fn translate_text<S>(
        &self,
        text: S,
        target_lang: TargetLanguage,
    ) -> Result<Option<Translation>>
    where
        S: ToString,
    {
        let request = TranslationRequest::new(text, target_lang);
        let result = self.translate_text_request(request)?;

        if let Some(translation) = result.first() {
            return Ok(Some(translation.clone()));
        }

        Ok(None)
    }

    pub fn translate_text_request(&self, req: TranslationRequest) -> Result<TranslationsResponse> {
        let response = self.request("/translate", req)?;

        match serde_json::from_reader(response.into_reader()) {
            Ok(json) => Ok(json),
            Err(err) => Err(Error::SerdeJsonError(err)),
        }
    }

    pub fn get_usage(&self) -> Result<Usage> {
        let result = self.request_without_query("/usage")?;

        match serde_json::from_reader(result.into_reader()) {
            Ok(usage) => Ok(usage),
            Err(err) => Err(Error::SerdeJsonError(err)),
        }
    }

    fn request<T, Q>(&self, url: T, query: Q) -> Result<ureq::Response>
    where
        T: ToString,
        Q: PrepareRequest,
    {
        let mut request = ureq::post(&format!("{}{}", self.req_url, &url.to_string())).set(
            "Authorization",
            &format!("DeepL-Auth-Key {}", self.auth_key),
        );

        request = query.prepare_request(request);

        match request.call() {
            Ok(response) => Ok(response),
            Err(err) => Err(Error::UReqError(err)),
        }
    }

    fn request_without_query<T: ToString>(&self, url: T) -> Result<ureq::Response> {
        self.request(url, EmptyRequest)
    }
}


#[cfg(test)]
mod deepl_tests {
    use crate::{DeepL, URL_API, URL_API_FREE};

    const FREE_API_TOKEN: &'static str = "15070220:fx";
    const API_TOKEN: &'static str = "15070220";

    #[test]
    fn test_new_free_token() {
        let expected = DeepL {
            auth_key: FREE_API_TOKEN.to_string(),
            req_url: URL_API_FREE
        };

        let result = DeepL::new(FREE_API_TOKEN);

        assert_eq!(result, expected);
    }

    #[test]
    fn test_new_non_free() {
        let expected = DeepL {
            auth_key: API_TOKEN.to_string(),
            req_url: URL_API,
        };
        let result = DeepL::new(API_TOKEN);

        assert_eq!(result, expected);
    }
}
