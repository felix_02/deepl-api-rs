use std::result;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    UReqError(ureq::Error),
    IoError(std::io::Error),
    SerdeJsonError(serde_json::Error),
}
