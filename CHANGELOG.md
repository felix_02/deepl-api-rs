# Changelog

## [Unreleased]
### Added

### Changed
- The trait `IntoQuerys` was replaced by the `PrepareRequest` trait

### Removed
- trait `IntoQuerys` was removed
- struct `Query` was removed

## [v0.1.0] - 30.04.0222
### Added
- Basic text translation

### Changed
N/A

### Removed
N/A
