# DeepL API

This crate provides a connection to the DeepL API for Rust.
An API token is required for use, and an account at DeepL is needed for this.
This can be created [here](https://www.deepl.com/pro-api?cta=header-pro-api). Currently it is only tested with the free version,
but the paid version should also work.

## Example

```rust
let deepl_client = deepl::DeepL::new("<API-token>");

let translation = deepl_client
    .translate_text("Hello world!", deepl::TargetLanguage::German)
    .unwrap();

assert_ne!(translation, None);
if let Some(translation) = translation {
    assert_eq!(translation.text(), "Hallo Welt!");
    assert_eq!(
        translation.detected_source_language(),
        &deepl::SourceLanguage::English
    )
}
```

## Contributing
I appreciate any contribution, before push please format the code with [rustfmt](https://github.com/rust-lang/rustfmt).
